# from django.shortcuts import render
from django.views.generic import ListView

try:
    from tags.models import Tag
except Exception:
    Tag = None


# Create your views here.
class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"
    paginate_by = 4

    def get_queryset(self):
        querystring = self.request.GET.get("q")
        if querystring is None:
            querystring = ""
        return Tag.objects.filter(name__icontains=querystring)
